import java.util.ArrayList;

public class lab8_4
{
	public static void main(String[] args) 
	{
        IntegerSet A = new IntegerSet();
        A.insertElement(23); A.insertElement(25); A.insertElement(30);
        System.out.println("A: "+A.toString());
        IntegerSet B = new IntegerSet();
        B.insertElement(23); B.insertElement(25); B.insertElement(45);
        System.out.println("B: "+B.toString());
        A.deleteElement(25);
        System.out.println("A delete 25: "+A.toString());
        System.out.println("A = B?: "+A.equals(B));
        IntegerSet C = new IntegerSet();
        C = IntegerSet.union(A,B);
        System.out.println("A || B: "+C.toString());
        C = IntegerSet.intersection(A,B);
        System.out.println("A && B: "+C.toString());
    }
}
class IntegerSet
{
    boolean[] myArray = new boolean[100];
    IntegerSet()
    {
        for(int i = 0; i<100; i++)
        {
            myArray[i] = false;
        }
    }
    public void insertElement(int i)
    {
        this.myArray[i]=true;
    }
    public void deleteElement(int i)
    {
        this.myArray[i]=false;
    }
    public String toString()
    {
        String Str="";
        for(int i = 0; i<100; i++)
        {
            if(this.myArray[i]==true) Str=Str.concat(String.valueOf(i)+" ");
        }
        return Str;
    }
    public static IntegerSet union(IntegerSet a, IntegerSet b)
    {
        IntegerSet c = new IntegerSet();
        for(int i = 0; i<100; i++)
        {
            c.myArray[i]=(a.myArray[i]||b.myArray[i]);
        }
        return c;
    }
    public static IntegerSet intersection(IntegerSet a, IntegerSet b)
    {
        IntegerSet c = new IntegerSet();
        for(int i = 0; i<100; i++)
        {
            c.myArray[i]=(a.myArray[i]&&b.myArray[i]);
        }
        return c;
    }
    public boolean equals(IntegerSet a)
    {
        boolean chck=true;
        for(int i = 0; i<100; i++)
        {
            if(this.myArray[i]!=a.myArray[i])
            {
                chck=false; break;
            }
        }
        return chck;
    }

}

