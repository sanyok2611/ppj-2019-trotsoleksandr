import java.util.ArrayList;

public class lab8_1
{
	public static void main(String[] args) 
	{
        Point p = new Point(3,4).translate(1,3).scale(0.5f);
        System.out.println(p.getX()+" "+p.getY());    
    }
}
class Point
{
    private float x,y;
    Point(float x, float y)
    {
        this.x=x;
        this.y=y;
    }
    Point()
    {
        this.x=0;
        this.y=0;
    }
    public float getX()
    {
        return this.x;
    }
    public float getY()
    {
        return this.y;
    }
    public Point translate(float x, float y)
    {
        this.x+=x;
        this.y+=y;
        return new Point(this.x,this.y);
    }
    public Point scale(float x)
    {
        this.x*=x;
        this.y*=x;
        return new Point(this.x,this.y);
    }
}

