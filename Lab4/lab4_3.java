import java.util.Scanner;
import java.io.File;

public class lab4_3
{
	public  static int countSubStr(String str, String Substr)
	{	
		int temp=0;
		for (int i=0; i<str.length(); i++)
		{
		if (str.charAt(i)==Substr.charAt(0) && i+Substr.length()<=str.length()) 
			{	
			int r=0;
			for (int j=0; j<Substr.length(); j++)
				if(str.charAt(i+j)!=Substr.charAt(j)) r=1;
			if (r==0) temp++;
			}
			
		}
		return temp;
	}
    

	public static void main(String[] args)  throws Exception 
	{
        File file = new File(args[0]);
        Scanner sc = new Scanner(file);
        String text = args[1];
        int S = 0;
        if(sc.hasNext())
        {
            String a = sc.next();
            S+=countSubStr(a, text);
        }
        System.out.println(S);
	}
}
