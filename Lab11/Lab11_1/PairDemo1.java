public class PairDemo1
{
    public static void main(String[] args)
    {
        String[] words = { "Ala", "ma", "psa", "i", "kota" };
        Pair<String> mm = new Pair<String>("First","Second");
        System.out.println("First = " + mm.getFirst());
        System.out.println("Second = " + mm.getSecond());
        mm.swap();
        System.out.println("First = " + mm.getFirst());
        System.out.println("Second = " + mm.getSecond());
    }
}


