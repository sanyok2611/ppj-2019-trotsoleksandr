import java.util.*;

public class Lab12_1
{
    public static void main(String[] args)
    {
        PriorityQueue<String> Lista = new PriorityQueue<String>();
        Scanner in = new Scanner(System.in);
        while(true)
        {
            String cmd = in.nextLine();
            if(cmd.equals("show")) show(Lista);
            if(cmd.equals("dodaj")) add(Lista);
            if(cmd.equals("nastepne")) rem(Lista);
            if(cmd.equals("zakoncz")) break;
        }
    }
    static void show(PriorityQueue<String> Lista)
    {
        for (String A : Lista) 
        {
            System.out.println(A);
        }
    }
    static void add(PriorityQueue<String> Lista)
    {
        System.out.println("Wpish zadanie");
        Scanner in = new Scanner(System.in);
        Lista.add(in.next());
    }
    static void rem(PriorityQueue<String> Lista)
    {
        Lista.remove();
    }
}
