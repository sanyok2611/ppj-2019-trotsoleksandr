import java.util.*;

public class Lab12_6
{
    public static void main(String[] args)
    {
        Stack<Integer> Cyfry = new Stack<Integer>();
        Scanner in = new Scanner(System.in);
        System.out.println("Wpisz liczbe!");
        int liczba = in.nextInt();
        while(liczba>0)
        {
            int k = liczba%10;
            Cyfry.push(k);
            liczba=(liczba-k)/10;
        }
        while(!Cyfry.empty())
        System.out.println(Cyfry.pop());
    }
}
