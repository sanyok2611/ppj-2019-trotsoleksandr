import java.util.*;
public class Lab12_7
{
    public static void main(String[] args)
    {
        Scanner in = new Scanner(System.in);
        int Liczba = in.nextInt();
        Set<Integer> Zbior = new HashSet<Integer>();
        for(int i = 2; i<Liczba; i++) Zbior.add(i);
        int i = 2;
        Iterator<Integer> it = Zbior.iterator();
        while(i<Math.sqrt(Liczba))
        {
            it = Zbior.iterator();
            while(it.hasNext())
            {
                int k = it.next();
                if(k%i==0 && k!=2 && k!=3) it.remove();
            }
            i++;
        }
        for(int a : Zbior) System.out.println(a);
    }
}
