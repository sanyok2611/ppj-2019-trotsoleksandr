import java.util.*;

public class Lab12_5
{
    public static void main(String[] args)
    {
        Stack<String> Slowa = new Stack<String>();
        Scanner in = new Scanner(System.in);
        while(true)
        {
            String a = in.nextLine();
            if(a.isEmpty()) break;
            else if(a.charAt(a.length()-1)=='.')
            {
                Slowa.push(a);
                if(!Slowa.empty()) System.out.println(firstUpperCase(removeLastCharacter(Slowa.pop())));
                while(!Slowa.empty())
                {
                    String tmp = Slowa.pop();
                    if(Slowa.empty()) System.out.println(firstLowerCase(tmp)+".");
                    else System.out.println(tmp);

                }
            }
            else Slowa.push(a);
        }

    }
public static String removeLastCharacter(String str) {
   String result = null;
   if ((str != null) && (str.length() > 0)) {
      result = str.substring(0, str.length() - 1);
   }
   return result;
}
public static String firstUpperCase(String word){
	if(word == null || word.isEmpty()) return ""; //или return word;
	return word.substring(0, 1).toUpperCase() + word.substring(1);
}
public static String firstLowerCase(String word){
	if(word == null || word.isEmpty()) return ""; //или return word;
	return word.substring(0, 1).toLowerCase() + word.substring(1);
}
}
